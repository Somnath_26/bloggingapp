set the size of varchar to 33 for salt
******************************************************************************************

tables
all the pk are auto incremented and all the parameters are null
user
 
 uid | username | Email | Bio   | image  | pswhash | salt | timestamp(DATE ON WHICH ACCOUNT WAS CREATED)
  Pk | varchar2 | email | varchar2| varchar2 | varchar2|varchar2|timestamp

 article

 articleid | uid | Title | Description | body
  pk       | fk  |varchar|varchar2     |varchar2
 comments

 commentid|articleid | userid | comment
    pk    | FK     | FK     | string

 Tag table

 articleid | uid | taggid             
      |         |      |
      |         |       ------->uids of tagged users pK
      |         --------------->uid of who tagged FK
      ------------------------->article on which users are tagged FK

FILE STRUCTURE (SCAFOLDING)

- CONFIG
   - passport.js configuration
   - env var config
- ROUTES
    - endpoints(crud)

     - /users 
     - /afterlogin
     - /profiles tags article insert comment 
     - /remover

- app.js



//users table creation
CREATE table users(uid INT AUTO_INCREMENT NOT NULL,
username VARCHAR(100) NOT NULL,
email VARCHAR(100) NOT NULL,
bio text,
pswdhash varchar(200) NOT NULL,
salt VARCHAR(200) NOT NULL,
timestamp datetime default CURRENT_TIMESTAMP(),
PRIMARY KEY (uid) 
 );




//|||||||||||||||||||||||||||
//article table creation
create table article(articleid INT  AUTO_INCREMENT NOT NULL,
uid INT NOT NULL,
title VARCHAR(100) NOT NULL,
description text,
body text,
timestamp datetime default CURRENT_TIMESTAMP() NOT NULL,
PRIMARY KEY (articleid),
FOREIGN KEY (`uid`) REFERENCES `users` (`uid`));



//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//comment table creation
create table comments(commentid INT  AUTO_INCREMENT NOT NULL,
uid INT NOT NULL,
articleid INT NOT NULL,
comment text NOT NULL,
timestamp datetime default CURRENT_TIMESTAMP() NOT NULL,
PRIMARY KEY (commentid),
FOREIGN KEY (`uid`) REFERENCES `users` (`uid`),
FOREIGN KEY (`articleid`) REFERENCES `article` (`articleid`)
);
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// tag table creation
create table tag(tagid INT NOT NULL,
uid INT NOT NULL,
articleid INT NOT NULL,
timestamp datetime default CURRENT_TIMESTAMP(),
FOREIGN KEY (`uid`) REFERENCES `users` (`uid`),
FOREIGN KEY (`tagid`) REFERENCES `users` (`uid`),
FOREIGN KEY (`articleid`) REFERENCES `article` (`articleid`));
//||||||||||||||||||||||||||

!!!!!!!!!!!!!!!!!Addd time stamp  here 
and then create the tag table 
);



ALTER TABLE users
MODIFY COLUMN pswdhash text;




ALTER TABLE article
ADD images text;



insert into article(uid,title,description,body,images) values(1,"vintage", "Canoeing in the wilderness of Minnesota’s Boundary Waters","BOUNDARY WATERS CANOE AREA WILDERNESS, Minn. — Every paddle stroke sprinkled water drops, reflecting the setting sun like sparklers across the black, glacier-carved lake.
Just a few hours earlier, I had been portaging on an ankle-deep muddy trail with that 55-pound canoe balanced over my head, shielding me from a chilly downpour.
  That contrast is the essence of the wilderness experience in Minnesota’s Boundary Waters. The physical effort required to explore its off-the-grid remoteness — including carrying a canoe solo on slippery, rocky trails — makes every worry evaporate like steam off woolen socks strung over a campfire.
    
     And once your only concerns become basic — keeping chipmunks away from the breakfast oatmeal or securing tarps against the wind whooshing through the woods — you have nothing to do but soak in the beauty.
      Boldly north
     
      Covering over one million acres along the Minnesota-Canada border, the Boundary Waters Canoe Area Wilderness protects more than a thousand lakes, rocky islets, and towering evergreen forests that are usually ice-free from May into October.
    
      There are plenty of walleyes, pikes and loons along its 1,200 miles (1,930 kilometers) of lily pad-lined canoe trails — but no electricity, no motors (except on a few big entry lakes), and no cell phone or Wi-Fi signals in the vast majority of the wilderness.
      If you want those, or a shower, bed and restaurant meals, there are plenty of nearby spartan-to-five-star lakeside cabins and lodges.  Deep inside the wilderness, the luxury is the silence, quieting everything to the same stillness of the glossy lake surfaces that mirror the bursts of stars or the spindly pine trees. Even planes cannot fly below 4,000 feet here.Into the wilderness
      My first canoeing and camping four-day trip in the Boundary Waters came as I was interviewing for jobs after finishing my Ph.D. I don’t believe that I ever looked at any object with as much loathing as I did the payphone outside the outfitters’ office when we got back to civilization.","https://images.pexels.com/photos/967292/pexels-photo-967292.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");