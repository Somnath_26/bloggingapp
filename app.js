var createError = require('http-errors');
const express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
//logger
var logger = require('morgan');
// respose configured
var compression = require('compression')
//body parser
var bodyParser = require('body-parser')
// local import route
var Router = require('./routes');
//db configuration
var connection = require('./config/dbconfig');
var cors = require('cors');
//helmet config
var helmet = require('helmet');



var app = express();
app.use(cors())
// database connect
try{
connection.connect(function(err) {
  if (err) throw err;
  
});}
catch(err){
  console.log("error connecting db")
}
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');



//middlewares*******************************************
//protection enabled  
app.use(helmet())
//body parser enabled
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
// response compression enable
app.use(compression())
// morgan the logger
app.use(logger('dev'));
// body-parser enabled
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
//cookie parser enabled
app.use(cookieParser());
// static file serving path configured
app.use(express.static(path.join(__dirname, 'public')));
// express router enabled
app.use('/routes', Router);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});
// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
