const router = require('express').Router();

router.use('/handlers', require('./handlers'));

module.exports = router;