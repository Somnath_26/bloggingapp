//************ the route is used to check wether email exists or not
const express = require("express");
const router = express.Router();
const mysql = require("mysql");
const connection = require("../../../config/dbconfig");
var crypto = require("crypto");

//new random password generator route                               ----[tested]----
router.post("/create", (req, res) => {
  let email = req.body.email;
  // check wether email exists in the user table
  try{
  let emailCheckUsers = function() {
    //***check one */
    return new Promise(function(resolve, reject) {
      connection.query(
        'SELECT COUNT(*) as records FROM users WHERE email = "' + email + '"',
        (error, results, fields) => {
          console.log(
            "======================================================================================================"
          );
          console.log(results);

          if (results[0].records) {
            console.log("Check 1 passed");
            resolve();
          } else {
            console.log("users check1 failed ['user is not registered']");
            res.json({pCreate:false})
          }
        }
      );
    });
  };

  // check wether email exists in the forgotpassword table     if exis handle accordingly!!!
  let emailCheckForgotPassword = function() {
    //***check two */
    return new Promise(function(resolve, reject) {
      connection.query(
        'SELECT * FROM forgotPassword where email="' + email + '"',
        (error, results, fields) => {
          if (!results.length) {
            console.log("check 2 passed empty array should be returned");
            console.log(results);
            resolve();
          } else {
            console.log("!!check 2 failed!!");

            console.log("the user has already genrated the random password");
            res.json({pCreate:false})
          }
        }
      );
    });
  };

  // //insert the query here
  let finalInsertion = function() {
    //***genrate random password and insert */
    return new Promise(function(resolve, reject) {
      // generate random string
      let randomPassword = crypto.randomBytes(4).toString("hex");
      console.log("random password : " + randomPassword);

      connection.query(
        'INSERT INTO forgotPassword (email,randomPassword) VALUES( "' +
          email +
          '","' +
          randomPassword +
          '")',
        (error, results, fields) => {
          if (error) throw error;
          {
            console.log(results);
            res.json({ pCreate: true });
            resolve(results);
          }
        }
      );
    });
  };

  // promises chaining sequence check1-->check-->2-->finally insert
  emailCheckUsers()
    .then(function(result) {
      return emailCheckForgotPassword(result);
    })
    .then(function(result) {
      return finalInsertion(result);
    })
    .then(function(result) {
      console.log(result);
    })

    .catch(error => {
      console.log(error);
      res.send(error);
      console.log("sending response .......");
    });

  }
  catch(error)
  {console.log("error occured the db server closed the connection")}

})

  // route to verify the user entered random password                             ----[tested]----
  router.post("/check", (req, res) => {
    if (req.body.randomPassword) {
      // select the emailid of the corresponding randompassword
      try{

      let randomPassword = req.body.randomPassword;
      console.log(randomPassword);

      connection.query(
        'SELECT email FROM forgotPassword WHERE randomPassword = "' +
          randomPassword +
          '"',
        (error, results, fields) => {
          if (error) throw error;
          if (results.length > 0) {
            console.log("randomPassword check Successful");
            res.json({
              user: true,
              email: results[0].email
            });
          } else {
            console.warn("user not found in the db");
            res.json({ user: false });
          }
        }
      );
    }
    catch(error){
      console.log("error occured")
    }


    }
  });

  
  
  
  // the user clicks on the received password recovery url                                [testing]
  // the forget password email id should be the  foreign key and unique

  router.post("/", (req, res) => {
    // check wether there is url payload and match the length with 4 bytes = 4*2 hex =8
    if (req.body.email) {
      let randomPassword = req.body.randomPassword;
      let email = req.body.email;
      let password = req.body.password;
      


      
      
      
      let getEmailForgotPassword = function() {
        return new Promise(function(resolve, reject) {
            connection.query('SELECT * FROM forgotPassword WHERE randomPassword = "' + randomPassword + '"', (error, results, fields) =>{
                if(error) throw error;
            
                if(results.length>0){
                    let dbemail=results[0].email;
                    if(dbemail===email){
                       // console.log(results)
                        resolve(results);

                        }
                        else{
                            console.log("email mismatch error")
                            res.json({error:true})
                        }

                }
                else{
                    console.log("wrong credentials --> randompassword mismatch error")
                }
            })

         
        });
      };

      
      let deleteCreds = function(results) {
        return new Promise(function(resolve, reject) {

            connection.query('DELETE FROM forgotPassword WHERE email ="' + dbemail + '"', (error, results, fields) => {
                if (error) throw error;
                console.warn("randompassword enty deleted!!!")
                console.log(results);
                resolve(results);
            });

            
        });
      };
      
      let updateCreds = function(message) {
        return new Promise(function(resolve, reject) {

            //generate salt
  let salt = crypto.randomBytes(16).toString('hex');
  //hasher the password
  let hash = crypto.pbkdf2Sync(password, salt, 10000, 512, 'sha512').toString('hex');
 //alter row in tablename where email='"+email+"'....set the new salt,pswdhash

 connection.query('UPDATE forgotPassword  SET salt="'+salt+'" ,pswdhash="'+hash+'" WHERE email="'+email+'" ', (error, results, fields) => {
    if (error) throw error;
    console.log(results);
    resolve("credentials updated")
    res.json({pReset:true})
    resolve("credentials updated")
});



     
        });
      };
      
      getEmailForgotPassword().then(function(result){
          return deleteCreds(result);
      }).then(function(result){
          return updateCreds(result);
      }).then(function(result){
          console.log(result);
      }).catch((error)=>{
          console.log("error occured operation unsucessful!!");
          res.send(error);
      })






    
    
    //   // [query] search the db for the random string and select the email
    //   connection.query(
    //     'SELECT * FROM forgotPassword WHERE randomPassword = "' +
    //       randomPassword +
    //       '"',
    //     (error, results, fields) => {
    //       if (error) throw error;
          
    //       if (results.length > 0) {
    //         console.log(results);
    //         let dbemail = results[0].email;
    //         //check wether the user entered email matches with db email
    //         if (dbemail === email) {
    //           // query to delete the random password and email entry from the forget password table

    //           connection.query(
    //             'DELETE FROM forgotPassword WHERE email ="' + dbemail + '"',
    //             (error, results, fields) => {
    //               if (error) throw error;
    //               console.warn("randompassword enty deleted!!!");
    //               console.log(results);
    //             }
    //           );

    //           //generate salt
    //           let salt = crypto.randomBytes(16).toString("hex");
    //           //hasher the password
    //           let hash = crypto
    //             .pbkdf2Sync(password, salt, 10000, 512, "sha512")
    //             .toString("hex");
    //           //alter row in tablename where email='"+email+"'....set the new salt,pswdhash

    //           connection.query(
    //             'UPDATE forgotPassword  SET salt="' +
    //               salt +
    //               '" ,pswdhash="' +
    //               hash +
    //               '" WHERE email="' +
    //               email +
    //               '" ',
    //             (error, results, fields) => {
    //               if (error) throw error;
    //               console.log(results);
    //               res.json({ pReset: true });
    //             }
    //           );
    //         } else {
    //           console.log("the entered email didnot match");
    //           res.json({ pReset: false });
    //         }
    //       } else {
    //         console.warn("user not found in the db");
    //         res.json({ user: false });
    //       }
    //     }
    //   );
    // } else {
    //   console.log("error!! not found or payload length is incorrect");
    //   res.json({ payload: "error" });
    }
    });


module.exports = router;
