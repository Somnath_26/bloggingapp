const router = require('express').Router();

// handlers configs 

router.use('/home',require('./home'))
router.use('/users', require('./users'));
router.use('/profile', require('./profile'));
router.use('/remover',require('./remover'));

// router.use('/home', require('./home'));
// router.use('/remover', require('./remover'));
//if server is not able to process the request
router.use(function(err, req, res, next){
  
  if(err.name === 'ValidationError'){
    return res.status(422).json({
      errors: Object.keys(err.errors).reduce(function(errors, key){
        errors[key] = err.errors[key].message;

        return errors;
      }, {})
    });
  }

  return next(err);
});

module.exports = router;