//************ the route is  /routes/handlers/api/login
const express = require("express");
const router = express.Router();
const connection = require("../../../config/dbconfig");
const crypto = require("crypto");
const jwt = require("jsonwebtoken");

//user login [POST]
router.post("/", (req, res) => {
  console.log(req.body);
  if (req.body.email && req.body.password) {
    //get the user data from the req
    let email = req.body.email;
    let password = req.body.password;

    connection.query(
      'SELECT COUNT(*) FROM users WHERE email = "' + email + '"',
      (error, results) => {
        if (error) throw error;

        if (results[0]["COUNT(*)"]) {
          //get the password hash and salt for the corresponding email
          connection.query(
            'SELECT * from users where email="' + email + '"',
            (error, results) => {
              if (error) throw error;
              // parse the hash and salt present in the db
              let dbhash = results[0].pswdhash;
              let dbsalt = results[0].salt;
              let dbuid = results[0].uid;
              let dbusername = results[0].username;
              //!!! Do it after implementing multer
              //let image =results[0].image;

              // now hash the req.body.password using the db salt
              let newhash = crypto
                .pbkdf2Sync(password, dbsalt, 10000, 512, "sha512")
                .toString("hex");

              //now compare newhash and db hash
              if (newhash === dbhash) {
                console.log("user is legit signing JWT Token......");

                //now genrate jwt token and send as response

                const today = new Date();
                const exp = new Date(today);
                let secret = "secret";
                exp.setDate(today.getDate() + 5);

                const token = jwt.sign(
                  {
                    id: dbuid,
                    username: dbusername,
                    exp: parseInt(exp.getTime() / 1000)
                  },
                  secret
                );

                res.json({
                  token: token
                });
              } else {
                console.log("not-legit!!!");
                res.json({
                  user: "false"
                });
              }
            }
          );
        } else {
          res.json({ user: "wrong username" });
          console.log("username not found in db!!!");
        }
      }
    );

    // ends here
  } else {
    console.log("payload was not received");
    console.log(req.body);
    res.json({ payload: "not received" });
  }
});

module.exports = router;
