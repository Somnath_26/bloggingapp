//************ the route is  /routes/handlers/api/login
const express = require("express");
const router = express.Router();
const connection = require("../../../config/dbconfig");
const jwt = require("jsonwebtoken");

//user login [POST]
router.get("/", (req, res) => {
  if (req.get("token")) {
    //get the token from header
    const token = req.get("token");
    console.log(token);
    // decode the token
    var decoded = jwt.verify(token, "secret");
    console.log(decoded);
    var uid = decoded.id;

    connection.query(
      'SELECT bio,timestamp FROM users WHERE uid = "' + uid + '"',
      (error, results) => {
        if (error) throw error;
        let timestamp = results[0].timestamp.toString();
        let date = timestamp.slice(0, 15);
        const day = date.slice(0, 3);
        const month = date.slice(4, 7);
        const dd = date.slice(8, 10);
        const year = date.slice(11, 15);
        const formatedDate = day + "-" + dd + "-" + month + "-" + year;

        res.json({
          date: formatedDate,
          bio: results[0].bio
        });
      }
    );
  } else {
    res.json({ error: true });
    console.log("token not received error while validating please sign in....");
  }
});

module.exports = router;
