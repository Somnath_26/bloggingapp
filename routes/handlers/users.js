// the route is  /routes/
const express = require('express')
const router = express.Router()

router.use('/bio',require('./bio'))
router.use('/forgotPassword',require('./forgotPassword'))
router.use('/register', require('./register'));
router.use('/login', require('./login'));
router.use('/bio',require('./bio'))

module.exports = router