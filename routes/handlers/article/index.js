//************ the route is  /routes/handlers/api/login
const express = require("express");
const router = express.Router();
const connection = require("../../../config/dbconfig");
const jwt = require("jsonwebtoken");
//user login [POST]
router.post("/", (req, res) => {
  if (req.get("token")) {
    try {
      //get the user data from the req
      let title = req.body.title; //blog title
      let description = req.body.description; //blog deswcription
      
      let tempbody = req.body.body;
      const encodedbody = new Buffer(tempbody).toString('base64');
      let body = encodedbody; //blog body
      let image = req.body.image; // image url
      //get the token from header
      const token = req.get("token");
      // decode the token
      var decoded = jwt.verify(token, "secret");
      console.log(decoded);
      let uid = decoded.id;
      connection.query(
        'INSERT INTO article (uid,title,description,body,images) VALUES("' +
          uid +
          '","' +
          title +
          '","' +
          description +
          '","' +
          body +
          '","' +
          image +
          '")',
        (error, results) => {
          if (error) throw error;
          console.log(results);
        }
      );
      console.log("success");
      res.json({ status: true });
    } catch (error) {
      console.log("the sent req");
      console.log(req.body);
      console.log("**********************************");
      console.log(error);
      res.json({
        req: req.body,
        error: "occured",
        status: false
      });
    }
  } else {
    res.json({
      payload: "not received"
    });
  }
});

router.get("/", (req, res) => {
  connection.query(
    "SELECT  images,title,description,articleid,uid FROM article",
    (error, results) => {
      if (error) throw error;
      console.log(results);
      res.json({
        blogs: results
      });
    }
  );
});

router.post("/blogbody", (req, res) => {
  let uid = req.body.uid;
  let articleid = req.body.articleid;
  console.log("look here");
  console.log(uid);
  connection.query(
    'SELECT  body,timestamp FROM article where articleid = "' + articleid + '"',
    (error, results) => {
      if (error) throw error;
      var tempblogBody = results[0].body;

      var blogBody =Buffer.from(tempblogBody, 'base64').toString('ascii');
      let timestamp = results[0].timestamp.toString();
      let date = timestamp.slice(0, 15);
      const day = date.slice(0, 3);
      const month = date.slice(4, 7);
      const dd = date.slice(8, 10);
      const year = date.slice(11, 15);
      const formatedDate = day + "-" + dd + "-" + month + "-" + year;

      // call the users table and get the username
      connection.query(
        'SELECT username FROM users where uid ="' + uid + '"',
        (error, results) => {
          if (error) throw error;
          var author = results[0].username;
          const output = {
            author: author,
            body: blogBody,
            timestamp: formatedDate
          };
          console.log(output);

          res.send({
            output
          });
        }
      );
    }
  );
});

module.exports = router;
