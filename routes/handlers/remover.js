// the route is  /routes/
const express = require('express')
const router = express.Router()


router.use('/delete', require('./delete'));


module.exports = router 