//this file contains db configurations needed to connect to the mysql server
// require this as connection and use| connection.connect();  |to start
//presently configured in the main app.js


var mysql      = require('mysql');

var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'newpassword',
    database : 'bloggingapp'
  });

  //connection.connect(); //start in app.js during node server initialization
  module.exports = connection
